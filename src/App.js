import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyApp from './components/MyApp'
import PostApi from './components/PostApi'
function App() {
  return (
    <div className="App">
      <PostApi />
      <MyApp/>
    </div>
  );
}

export default App;
