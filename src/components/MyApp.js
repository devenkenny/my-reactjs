import React, { Component } from 'react'
import axios from 'axios'
class MyApp extends Component {

    constructor(props) {
        super(props) 
    
        this.state = {
            post:""
        }
    }

    componentDidMount(){

        axios.get('http://35.154.48.29:8017/db/getMessageForDb')
        .then(response =>{
            console.log(response)
            this.setState({post: response.data})
        })
        .catch(error =>{
            console.log(error)
        })
    }


    render() {
        const{post} = this.state
        return (
            <div>
                   {post}
            </div>
        )
    }
}

export default MyApp
