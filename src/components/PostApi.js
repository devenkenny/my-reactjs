import React, { Component } from 'react'
import axios from 'axios'
class PostApi extends Component {

constructor(props) {
    super(props)

    this.state = {
         name : "",
         city : ""
    }
}

changeHandler= (e)=>{

       this.setState({[e.target.name] : e.target.value})


}
submitHandler = (e)=>{
    e.preventDefault()
    axios.post('http://35.154.48.29:8017/db/putDataInDb',this.state)
    .then(response =>{
        console.log(response)
    })
    .catch(error=>{
        console.log(error)
    })
}


    render() {
        return (
            <div>
                <form onSubmit={this.submitHandler}>
                <div>
                <label>name</label>
                <input type="text" name="name" value={this.state.name} onChange={this.changeHandler}></input>
                </div>
                <div>
                <label>city</label>
                <input type="text" name="city" value={this.state.city} onChange={this.changeHandler}></input>
                </div>
                <div>
                <button type="submit">submit</button>
                </div>
                </form>
            </div>
        )
    }
}


export default  PostApi
